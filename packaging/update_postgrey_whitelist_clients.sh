#!/bin/bash

#####################################################################
# Writes an updated postgrey_whitelist_clients to the deploy folder #
#####################################################################

scriptDir=$(dirname -- "$(readlink -f -- "$BASH_SOURCE")")
echo $scriptDir
wget -q -N http://postgrey.schweikert.ch/pub/postgrey_whitelist_clients -O $scriptDir/../deploy/postgrey_whitelist_clients
